# Signing Credentials using vault instead of transferring private keys

**Merge Request for Proposed Solution: [Merge Request](https://gitlab.com/gaia-x/lab/compliance/gaia-x-notary-registrationnumber/-/merge_requests/58/diffs)**

## Table of Contents

1. **Introduction**
2. **Creating X.509 Certificate**
   - 2.1 Generating CSR using Hashicorp Vault Key Pair
   - 2.2 Generating CSR using Azure Key Vault
   - 2.3 Issue Certificate from CSR
   - 2.4 Exposing X.509 Certificates
3. **Creating a `did:web` for Issuer**
   - 3.1 Exposing `did.json`
4. **Verifiable Credential Signing**
   - 4.1 Required Libraries/SDKs
   - 4.2 Flow Chart of Signing using Vault
5. **Validate Verifiable Credential**
   - 5.1 Verify VC using `@gaia-x/json-web-signature-2020`
   - 5.2 Verify VC-JWT using Vault

## 1. Introduction

In the evolving landscape of digital security, maintaining the confidentiality and integrity of cryptographic keys is paramount. Our project leverages the robust security features of Vault technologies—specifically HashiCorp Vault and Azure Key Vault—instead of hosting private keys directly in environmental configurations. This approach aligns with the secure management of Verifiable Credential Signature [presentation](https://gitlab.com/gaia-x/gaia-x-community/open-source-community/-/blob/main/minutes_weekly_meeting/2024/presentations/OSS%20Meeting%2028032024%20-%20VC%20Proof%20Proposal.pdf?ref_type=heads).

### Project Components:

1. **X.509 Certificate Generation:**

   - To comply with the requirements of `did:web` identifiers, we utilize X.509 certificates. Our process begins with the creation of a Certificate Signing Request (CSR). Upon generating the CSR, we employ tools like Certbot to automate the issuance of X.509 certificates, ensuring a seamless certification lifecycle.

2. **DID Document Creation:**

   - With the X.509 certificate in place, we proceed to construct a `did.json` document using our custom [signer tool](https://signertool.dev.smart-x.smartsenselabs.com/docs/#/default/post_v1_create_web_did). This document is crucial for establishing a verifiable digital identity on the web. Once generated, the `did.json` is hosted to ensure its accessibility and validity.

3. **Verifiable Credentials Signing:**
   - Our project enhances the Gaia-X Notary service by enabling verifiable credential signing using Azure and HashiCorp Vault instead of traditional private key in configs. While we utilize the legacy JWS-2020 format with Azure to adhere to current system, our integration with HashiCorp Vault employs the more secure and modern VC-JWT format, aligning with the latest W3C standards. This approach ensures robust security and flexibility in our verifiable credentials.
4. **Validate Verifiable Credential:**
   - For verifiable credentials issued by Azure, we use the `@gaia-x/json-web-signature-2020`, which is specifically designed to ensure compliance with the latest web standards. This allows us to securely confirm that the credentials have not been tampered with and are still valid under the terms they were issued.
   - For credentials signed with HashiCorp Vault, we employ the Vault's API to perform verification on VC-JWT. HashiCorp Vault's security model ensures that the keys used for signing the credentials are securely managed and access-controlled.

## 2. Creating X.509 Certificate

### 2.1 Generating CSR using Hashicorp Vault Key Pair

#### Generate a Key Pair

To generate an RSA key pair that will be used to create an unsigned CSR:

```bash
openssl genpkey -algorithm RSA -out private.key
```

#### Create Configuration File for CSR

Create a configuration file for generating the CSR:

```bash
nano unsigned-csr.cnf
```

```javascript
[ req ]
default_bits       = 2048
prompt             = no
default_md         = sha256
distinguished_name = dn
req_extensions     = req_ext

[ dn ]
C  = FR
ST = Nouvelle-Aquitaine
L  = Bordeaux
O  = Gaia-X Dev
OU = CTO Team
CN = dev.gaia-x.eu

[ req_ext ]
basicConstraints = CA:TRUE
keyUsage = critical, keyCertSign, digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
extendedKeyUsage = serverAuth, clientAuth, codeSigning, emailProtection, timeStamping
nsCertType = client, server, email, objsign, sslCA, emailCA, objCA
subjectAltName = @alt_names
subjectKeyIdentifier = hash

[ alt_names ]
DNS.1 = localhost
IP.1  = 127.0.0.1
```

#### Generate Unsigned CSR

Generate an unsigned CSR using the previously created private key and configuration file:

```bash
openssl req -new -key private.key -out request.csr -config unsigned-csr.cnf
```

#### Convert CSR to PEM Format

Convert the CSR into a PEM file for further processing:

```bash
openssl req -in request.csr -out csr.pem
```

#### Start HashiCorp Vault

Run a HashiCorp Vault server using Docker:

```bash
docker run -p 8200:8200 -e 'VAULT_DEV_ROOT_TOKEN_ID=<your-token>' hashicorp/vault
```

#### Access Vault UI

Open the HashiCorp Vault UI in a browser:

```
http://localhost:8200/ui/vault/secrets
```

#### Enable Transit Engine and Create Key Pair

- Enable the Transit secrets engine.
- Create an RSA 2048 key pair with a specific name.

#### Sign CSR through HashiCorp Vault

Sign the CSR using the Transit secrets engine via the API:

```curl
curl --header "X-Vault-Token: <your-token>" --request POST --data '{"csr": "csr.pem as string"}' https://vault.example.com/v1/transit/keys/{keyname}/sign
```

<!-- ```javascript
import NodeVault from "node-vault";
const fs = require("fs");

class JWT {
  async getKey() {
    const keyName = "rsa-256";
    const certPem = fs.readFileSync("csr.pem", "utf8");
    const csr = await this.signCSR(keyName, certPem);
    console.log(csr);
  }

  async signCSR(keyName: string, csr: string): Promise<string> {
    const vault = NodeVault({
      apiVersion: "v1",
      endpoint: "http://0.0.0.0:8200",
      token: "<your-token>",
    });
    const resp = await vault.write(`transit/keys/${keyName}/csr`, { csr });
    if (resp?.data?.csr) {
      return resp.data.csr;
    }
    console.log(resp);
    throw new Error("invalid response data from vault");
  }
}

const main = new JWT();
main.getKey();
``` -->

#### Retrieve Signed CSR

The response from HashiCorp Vault will include the signed CSR:

```javascript
{
    "data": {
        "name": "my-key",
        "version": 1,
        "csr": "-----BEGIN ..."
    }
}
```

### 2.2 Generating CSR using Azure Key Vault

Generate CSR Certificate from Azure Key Vault using following this **[blog](https://learn.microsoft.com/en-us/azure/key-vault/certificates/create-certificate-signing-request?tabs=azure-portal)**

### 2.3 Issue Certificate from CSR

<img src="./docs/cert-gen.png">

#### Issue a certificate using the signed CSR:

```bash
sudo certbot certonly --manual --csr ./certificate.csr
```

#### Steps to Complete the Certificate Issue:

Certbot will display a challenge that you need to pass. This involves placing a specific file on your web server.
Create the file in the specified location with the content provided by Certbot.
Verify File Accessibility:

- Ensure the challenge file is accessible at the URL provided by Certbot, e.g., http://<yourdomain.com>/.well-known/acme-challenge/<filename>.

- Once the challenge file is verified to be accessible via your web browser, return to your terminal and press ENTER to complete the verification process.

##### sample code to host file and complete challenge

- File Creation: Certbot will provide a filename (`a-string`) and a challenge code (`a-challenge`). You need to create a file named `a-string` and place the challenge code inside it.
- Directory Structure: Create the following directory structure on your server:

```diff
\\server
----\\.well-known
--------\\acme-challenge
------------a-string
----server.js
```

- Host the Challenge: Use Node & Express to serve the file. Below is a simple server setup:

```javascript
const express = require("express");
const app = express();

app.use(express.static(__dirname, { dotfiles: "allow" }));

app.listen(80, () => {
  console.log("HTTP server running on port 80");
});
```

- Place the `a-string` file inside the `public/.well-known/acme-challenge/` directory.

- Verify the File Accessibility: Verify that the file is accessible via your web browser at:

```vbnet
http://yourdomain.com/.well-known/acme-challenge/a-string
```

- After successful verification, Certbot will issue the SSL certificate, which you can then use to secure your server.

### 2.4 Exposing X.509 Certificates

#### Enhancing Trust and Security:

X.509 certificates are vital for ensuring security and trust. They use a public-private key pair to authenticate identities and encrypt communications, supporting secure interactions and data exchange. By embedding X.509 certificates in did.json documents, we enhance the reliability and verification of Verifiable Credentials and allow anyone to verify our certificate and cross-check with Trust Anchors.

## 3. Creating a `did:web` for Issuer

### 3.1 Exposing `did.json`

#### Importance and Function:

Hosting the did.json document on your domain enables others to easily discover and verify your public keys and service endpoints. This document is crucial for the DID resolution process, as it allows resolvers to translate the did:web into a URL where the descriptor can be retrieved, ensuring reliable verification of digital identities on the web.

##### Example Configuration in did.json:

```json
{
  "@context": ["https://www.w3.org/ns/did/v1"],
  "id": "did:web:did-new.learn.smartsenselabs.com",
  "verificationMethod": [
    {
      "@context": "https://w3c-ccg.github.io/lds-jws2020/contexts/v1/",
      "id": "did:web:did-new.learn.smartsenselabs.com#azure",
      "type": "JsonWebKey2020",
      "controller": "did:web:did-new.learn.smartsenselabs.com",
      "publicKeyJwk": {
        "kty": "RSA",
        "n": "qoRBfIJrFeexVibzAg9oSTGKltWQh6amCA1je_6lQLarN_ZoyaPCHdqXsNkGTAEvdLYz7_6U72e-CCxQC2qHjYNYwMh5NLvaZ1Z6pPln7vXSfP6W6HGe2EZ-s9wh0CVFicYNK6EDHSv_krMatB0mm4sgVZf9lCqkvzVBY3awjCgrhJYR9ZM15i16ohpJERRFIGS0pZ0Zu5v9KUHcqLVOKYinF2zvkJalwR7rYvqLf3BkWpFNhvpA3L3PvFWeYXVpPqy2KhKpmls4de4E6dmDsxGcREyJOHWod-qflKKer70-ehaNeawa1IzEsw7ySymra2Wl4m1g51T6eu1GZuRLAQ",
        "e": "AQAB",
        "alg": "PS256",
        "x5u": "https://did-new.learn.smartsenselabs.com/.well-known/azure.pem"
      }
    },
    {
      "@context": "https://w3c-ccg.github.io/lds-jws2020/contexts/v1/",
      "id": "did:web:did-new.learn.smartsenselabs.com#hashicorp",
      "type": "JsonWebKey2020",
      "controller": "did:web:did-new.learn.smartsenselabs.com",
      "publicKeyJwk": {
        "kty": "RSA",
        "n": "sr6QDToXvit4JyCBV712avu-3eTOyR_cZ1PffrWTfUxSn-OY0Nfk0I_39-nJedH3IWn5P31acFw1AWa2xulv1p-lPbsBj1fDf31D7eicgf3iZkvE7T1qihxjOuIhvIOgxfXT9Nd84ef2ZX0QhQXAF5RCN2gJdJ9okvbKjj-c5ANynhk2jjc0LNzFwFqfDjQofEN6IYLiwpY1o--ZN4rgiahEzFVwhA7BAR5R6ttJm803djBgAOS8S4O5SFS_TyJj_zlgiijaOx3U9hpS5GEzorL58XN9u6wlwLjLuXVtHBMbfgi3jvKSwl6dg51LDHt1CdK8sL8kz64pLrhMrKyL5Q",
        "e": "AQAB",
        "alg": "PS256",
        "x5u": "https://did-new.learn.smartsenselabs.com/.well-known/hashicorp.pem"
      }
    }
  ],
  "assertionMethod": [
    "did:web:did-new.learn.smartsenselabs.com#azure",
    "did:web:did-new.learn.smartsenselabs.com#hashicorp"
  ]
}
```

## 4. Verifiable Credential Signing

### 4.1 Required Libraries/SDKs

To implement verifiable credential signing in Notary Service, we utilize specific libraries and SDKs that facilitate interactions with Azure Key Vault and HashiCorp Vault. Here’s what we use and why:

- @azure/identity: This library provides Azure Active Directory token authentication support across the Azure SDK. It is essential for authenticating to Azure Key Vault, allowing secure handling and retrieval of keys.

- @azure/keyvault-keys: Used to manage keys, cryptographic operations, and secrets in Azure Key Vault. This library is crucial for operations such as key wrapping, encryption, decryption, and, importantly for our purposes, signing credentials.

- Axios for HashiCorp Vault: While not a library specifically designed for Vault. In our setup, we use HashiCorp Vault's API for operations such as signing.

<img src="./docs/signing-vault.png">

### 4.2 Flow Chart of Signing using Vault

<img src="./docs/flow.png">

## 5. Validate Verifiable Credential

Verifying the authenticity and integrity of verifiable credentials is essential for ensuring trust . Our system employs two methods for validation depending on the signing method used to issue the credential.

### 5.1 Verify VC using @gaia-x/json-web-signature-2020

For credentials issued and signed with JSON Web Signature 2020 compliant standards used by Gaia-X, we utilize the @gaia-x/json-web-signature-2020 library. This library is specifically designed to handle and verify JSON Web Signatures (JWS) based on the specifications laid out in 2020, ensuring that the credentials are secure and tamper-proof.

### 5.2 Verify VC-JWT using Vault

For credentials signed using HashiCorp Vault, we verify them using the Vault’s API, which provides a secure mechanism to validate JSON Web Tokens (JWTs) signed by the Vault.
