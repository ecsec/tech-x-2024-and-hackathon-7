from jwcrypto import jwk
import config as config
import schema as schema
import uuid
import requests
from jwcrypto.jwk import JWK
import utils as utils
import hashlib
import datetime


class CredentialService:
    private_key_path: str
    issuer: str
    id_prefix: str

    def __init__(self, private_key_path: str, issuer: str, id_prefix: str):
        self.private_key_path = private_key_path
        self.issuer = issuer
        self.id_prefix = id_prefix

    def get_private_key(self) -> JWK:
        private_key = jwk.JWK()
        with open(self.private_key_path, "rb") as f:
            private_key.import_from_pem(data=f.read(), password=None)
            return private_key

    def create_legal_participant_credential(self, legal_registration_number: str, legal_name: str,
                                            country_subdivision_code: str) -> dict:
        """

        """
        legal_participant_vc: dict = schema.LEGAL_PARTICIPANT
        legal_participant_vc["id"] = '{}/{}.json'.format(self.id_prefix, uuid.uuid4())
        legal_participant_vc["issuer"] = self.issuer
        legal_participant_vc["credentialSubject"]["id"] = '{}/{}.json'.format(self.id_prefix, uuid.uuid4())
        legal_participant_vc["credentialSubject"]["gx:legalRegistrationNumber"]["id"] = legal_registration_number
        legal_participant_vc["credentialSubject"]["gx:legalName"] = legal_name
        legal_participant_vc["credentialSubject"]["gx:headquarterAddress"]["gx:countrySubdivisionCode"] = country_subdivision_code
        legal_participant_vc["credentialSubject"]["gx:legalAddress"]["gx:countrySubdivisionCode"] = country_subdivision_code
        return legal_participant_vc

    def issue_registration_number(self, vat_id: str) -> dict:
        """

        """
        payload = {'vcid': '{}/{}.json'.format(self.id_prefix, uuid.uuid4())}
        body = {
            "@context": [
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/participant"
            ],
            "type": "gx:legalRegistrationNumber",
            "id": '{}/{}.json'.format(self.id_prefix, uuid.uuid4()),
            "gx:vatID": vat_id
        }
        response = requests.post(config.NOTARY_HOST_URL, params=payload, json=body)
        response_json: dict = response.json()
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, response_json)
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, response_json['credentialSubject'])
        return response_json

    def issue_participant_credential(self, lrn_vc: dict, legal_name: str, country_subdivision_code: str) -> dict:
        # Get private key
        private_key: JWK = self.get_private_key()
        # Request registration number
        # Create legal participant credential
        vc_participant = self.create_legal_participant_credential(lrn_vc['credentialSubject']['id'], legal_name,
                                                                  country_subdivision_code)
        # Sign credential
        lp_vc = utils.sign_doc(vc_participant, private_key, config.ISSUER_KEY)
        # Store credential
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, lp_vc)
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, lp_vc['credentialSubject'])
        print("All process executed successful.")
        return lp_vc
    
    def issue_terms_and_conditions(self) -> dict:
        # Get private key
        private_key: JWK = self.get_private_key()
        
        tc_doc = {
            "@context": [
                "https://www.w3.org/ns/credentials/v2",
                "https://w3id.org/security/suites/jws-2020/v1",
                "https://registry.lab.gaia-x.eu/development/api/trusted-shape-registry/v1/shapes/jsonld/trustframework#"
            ],
            "id": '{}/{}.json'.format(self.id_prefix, uuid.uuid4()),
            "issuer": self.issuer,
            "type": "VerifiableCredential",
            "credentialSubject": {
                "gx:termsAndConditions": "The PARTICIPANT signing the Self-Description agrees as follows:\n- to update its descriptions about any changes, be it technical, organizational, or legal - especially but not limited to contractual in regards to the indicated attributes present in the descriptions.\n\nThe keypair used to sign Verifiable Credentials will be revoked where Gaia-X Association becomes aware of any inaccurate statements in regards to the claims which result in a non-compliance with the Trust Framework and policy rules defined in the Policy Rules and Labelling Document (PRLD).",
                "type": "gx:GaiaXTermsAndConditions",
                "id": '{}/{}.json'.format(config.ID_PREFIX_PARTICIPANT, uuid.uuid4()),
            }
        }

        tc_vc = utils.sign_doc(tc_doc, private_key, config.ISSUER_KEY)
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, tc_vc)
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, tc_vc['credentialSubject'])
        return tc_vc

    def issue_service_offering(self, lp_credentialsubject_id, name: str, serviceEndpointUrl: str, termsAndConditionsUrl: str, requestType: str = "API", formatType: str = "application/json", policy: str = "default allow = true") -> dict:
        service_offering_vc: dict = schema.SERVICE_OFFERING
        service_offering_vc["id"] = '{}/{}.json'.format(self.id_prefix, uuid.uuid4())
        service_offering_vc["issuer"] = self.issuer
        credential_subject = service_offering_vc["credentialSubject"]
        credential_subject["id"] = '{}/{}.json'.format(self.id_prefix, uuid.uuid4())
        credential_subject["gx:providedBy"]["id"] = lp_credentialsubject_id
        credential_subject["gx:name"] = name
        credential_subject["gx:serviceOffering:serviceEndpoint"] = serviceEndpointUrl
        credential_subject["gx:termsAndConditions"]["gx:URL"] = termsAndConditionsUrl
        credential_subject["gx:termsAndConditions"]["gx:hash"] = hashlib.sha256(termsAndConditionsUrl.encode()).hexdigest();
        credential_subject["gx:policy"] = policy
        credential_subject["gx:dataAccountExport"]["gx:requestType"] = requestType
        credential_subject["gx:dataAccountExport"]["gx:formatType"] = formatType

        so_vc = utils.sign_doc(service_offering_vc, self.get_private_key(), config.ISSUER_KEY)
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, so_vc)
        utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, so_vc['credentialSubject'])
        return so_vc

    def issue_vp_legal_participant(self, lrn_vc:dict, lp_vc: dict, tc_vc: dict):
        return self._issue_vp([lrn_vc, lp_vc, tc_vc])

    def issue_vp_service_offering(self, lrn_vc:dict, lp_vc: dict, tc_vc: dict, so_vc: dict):
        return self._issue_vp([lrn_vc, lp_vc, tc_vc, so_vc])
    
    def push_to_credential_event_service(self, vp: dict):
        dt = datetime.datetime.now(tz=datetime.timezone.utc).isoformat()

        body = {
        "specversion": "1.0",
        "type": "eu.gaia-x.credential",
        "source": "/hackathon7",
        "time": dt,
        "datacontenttype": "application/json",
        "data": vp
        }
        r = requests.post(config.CES_HOST_URL, json=body)
        return r.headers.get("Location")


    def _issue_vp(self, vc_list: list):
        payload = {'vcid': '{}/{}.json'.format(self.id_prefix, uuid.uuid4())}

        body = {
            "@context": "https://www.w3.org/ns/credentials/v2",
            "type": "VerifiablePresentation",
            "verifiableCredential": vc_list
        }
        r = requests.post(config.COMPLIANCE_HOST_URL, params=payload, json=body)
        compliance = None
        if r.status_code < 300:
            compliance = r.json()
            utils.upload_doc(config.POST_CREDENTIAL_HOST_URL, config.POST_CREDENTIAL_API_KEY, compliance)
        else:
            print(r.json())

        return compliance