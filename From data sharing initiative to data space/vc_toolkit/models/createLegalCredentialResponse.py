from pydantic import BaseModel


class CreateLegalCredentialResponse(BaseModel):
    lrn_vc_id: str
    lp_vc_id: str
    tc_vc_id: str
    lp_vp_id: str

