* [Presentation slides](https://gitlab.com/gaia-x/gaia-x-community/gx-hackathon/tech-x-2024-and-hackathon-7/-/blob/main/Advancing%20the%20LinkML-based%20CI%20infrastructure%20of%20WG%20Service%20Characteristics/2024-05_Gaia-X_Hackathon_LinkML_CI.pdf)
* Features implemented:
  * Support `equals_string_in` instead of always having to define an `enum` type: [issue](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/issues/309), [merge request](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/merge_requests/254), [contribution to LinkML](https://github.com/linkml/linkml/pull/2125)
  * Merge LinkML-generated SHACL/OWL with manually maintained SHACL shapes / OWL ontologies that use advanced SHACL/OWL features: [issue](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/issues/105), [merge request](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/merge_requests/252/diffs)
* Work in progress:
  * Adding superclasses/superproperties to shapes to be able to use superclasses as ranges of properties: [issue](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/issues/308), [merge request](https://gitlab.com/gaia-x/technical-committee/service-characteristics/-/merge_requests/253) 

We won the 3rd prize 😊